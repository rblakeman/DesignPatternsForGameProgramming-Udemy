﻿using Unity.Entities;

public class Spawner : IComponentData
{
    public Entity Prefab;
    public int ECSrows;
    public int ECScols;
}
