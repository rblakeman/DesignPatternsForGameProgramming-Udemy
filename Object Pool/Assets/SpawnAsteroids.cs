﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnAsteroids : MonoBehaviour
{
    public GameObject asteroidPrefab;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Random.Range(0, 1000) < 5) {
            Vector3 randomPos = this.transform.position + new Vector3(Random.Range(-10, 10), 0, 0);
            // Instantiate(asteroidPrefab, randomPos, Quaternion.identity);
            GameObject a = Pool.singleton.Get("asteroid");
            if (a != null) {
                a.transform.position = randomPos;
                a.SetActive(true);
            }
        }

    }
}
