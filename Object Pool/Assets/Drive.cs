﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Drive : MonoBehaviour
{
    public float speed = 10.0f;
    public GameObject bulletPrefab;
    public Slider healthbar;

    void Update()
    {
        float translation = Input.GetAxis("Horizontal") * speed;
        translation *= Time.deltaTime;
        transform.Translate(translation, 0, 0);

        if (Input.GetKeyDown("space")) {
            // Instantiate(bulletPrefab, this.transform.position, Quaternion.identity);
            GameObject b = Pool.singleton.Get("bullet");
            if (b != null) {
                b.transform.position = this.transform.position;
                b.SetActive(true);
            }
        }

        Vector3 screenPos = Camera.main.WorldToScreenPoint(this.transform.position);
        healthbar.transform.position = screenPos + new Vector3(0, -140f, 0);
    }

    private void OnCollisionEnter2D(Collision2D other) {
        if (other.gameObject.tag == "asteroid") {
            other.gameObject.SetActive(false);
            healthbar.value = healthbar.value - 10;

            if (healthbar.value <= 0) {
                SceneManager.LoadScene("ObjectPooling");
            }
        }
    }
}