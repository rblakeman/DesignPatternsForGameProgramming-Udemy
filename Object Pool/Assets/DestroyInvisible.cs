﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyInvisible : MonoBehaviour
{
    private void OnBecameInvisible() {
        if (this.gameObject.tag == "bullet" || this.gameObject.tag == "asteroid") {
            this.gameObject.SetActive(false);
        } else {
            Destroy(this.gameObject);
        }
    }
}
