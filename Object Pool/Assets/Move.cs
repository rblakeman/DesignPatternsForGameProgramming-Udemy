﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour
{
    public Vector3 velocity;

    // Update is called once per frame
    void Update()
    {
        this.transform.Translate(velocity);
    }

    private void OnCollisionEnter2D(Collision2D other) {
        if (other.gameObject.tag == "asteroid") {
            // Destroy(other.gameObject);
            // Destroy(this.gameObject);
            other.gameObject.SetActive(false);
            this.gameObject.SetActive(false);
        }
    }
}
