﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PoolItem {
    public GameObject prefab;
    public int max;
    public bool expandable;
}

public class Pool : MonoBehaviour
{
    public static Pool singleton;
    public List<PoolItem> items;
    public List<GameObject> pooledItems;

    void Awake() {
        singleton = this;
    }

    public GameObject Get(string tag) {
        foreach(GameObject poolItem in pooledItems) {
            if (!poolItem.activeInHierarchy && poolItem.tag == tag) {
                return poolItem;
            }
        }

        foreach(PoolItem item in items) {
            if (item.prefab.tag == tag && item.expandable) {
                GameObject obj = Instantiate(item.prefab);
                obj.SetActive(false);
                pooledItems.Add(obj);
                return obj;
            }
        }

        return null;
    }

    // Start is called before the first frame update
    void Start()
    {
        pooledItems = new List<GameObject>();
        foreach(PoolItem item in items) {
            for(int i = 0; i < item.max; i++) {
                GameObject obj = Instantiate(item.prefab);
                obj.SetActive(false);
                pooledItems.Add(obj);
            }
        }
    }
}
