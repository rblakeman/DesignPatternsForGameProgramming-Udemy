# DesignPatternsForGameProgramming-Udemy

> Unity Version 2019.3.8f1

## Design Patterns for Game Programming using Unity
### Course by: Penny de Byl
#### https://www.udemy.com/share/101WZq3@dkMfrHp1VRi4D28XKgrUiH0-1A1x_DRZixyBMG-bu2j7cklvm6rDtOntHvlUgEQv/



Course Content:
- Section 1: Introduction
- Section 2: The Command Pattern
- Section 3: The Flyweight Pattern
- Section 4: The Observer Pattern
- Section 5: The Prototype Pattern
- Section 6: The Singleton Pattern
- Section 7: The State Pattern
- Section 8: The Object Pool

Projects:
- Command Pattern (Section 2)
- Flyweight Pattern - Investigating Memory (Section 3)
- Flyweight Pattern - Scriptable Objects (Section 3)
- Flyweight Pattern - Unity ECS (Section 3) [Unity Version 2019.2.0a9](https://unity3d.com/unity/alpha/2019.2.0a9)
- Observer Pattern (Section 4)
- Prototype Pattern (Section 5)
- Singleton Pattern (Section 6)
- State Pattern (Section 7)
- Object Pool (Section 8)

Ryan Blakeman ©2021

rblakeman31@gmail.com

https://ryanblakeman.com
