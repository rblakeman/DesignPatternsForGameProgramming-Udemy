﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MessageController : MonoBehaviour
{
    Text itemPickedUpMessage;

    // Start is called before the first frame update
    void Start()
    {
        itemPickedUpMessage = this.GetComponent<Text>();
        itemPickedUpMessage.enabled = false;
    }

    public void DisplayMessage(GameObject gameObject) {
        itemPickedUpMessage.text = "You picked up an item!";
        itemPickedUpMessage.enabled = true;

        Invoke("TurnOffMessage", 3f);
    }

    void TurnOffMessage() {
        itemPickedUpMessage.enabled = false;
    }
}
