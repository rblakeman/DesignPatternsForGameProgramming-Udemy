﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Spawner : MonoBehaviour
{
    public GameObject eggPrefab;
    // public Image eggIcon;
    public GameObject boxPrefab;
    public Terrain terrain;
    TerrainData terrainData;
    // public Event eggSpawn;

    // Start is called before the first frame update
    void Start()
    {
        terrainData = terrain.terrainData;
        InvokeRepeating("CreateEgg", 1, 0.2f);
        InvokeRepeating("CreateBox", 1.1f, 0.2f);
    }

    void CreateEgg() {
        int x = (int) Random.Range(0, terrainData.size.x);
        int z = (int) Random.Range(0, terrainData.size.z);
        Vector3 pos = new Vector3(x, 0, z);
        pos.y = terrain.SampleHeight(pos) + 10;

        GameObject newEgg = Instantiate(eggPrefab, pos, Quaternion.identity);
        // eggSpawn.Occured(newEgg, eggIcon);
    }

    void CreateBox() {
        int x = (int) Random.Range(0, terrainData.size.x);
        int z = (int) Random.Range(0, terrainData.size.z);
        Vector3 pos = new Vector3(x, 0, z);
        pos.y = terrain.SampleHeight(pos) + 10;

        GameObject newBox = Instantiate(boxPrefab, pos, Quaternion.identity);
    }
}
