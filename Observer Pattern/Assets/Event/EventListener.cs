﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[System.Serializable]
// public class UnityGameObjectEvent : UnityEvent<GameObject, Image> { }
public class UnityGameObjectEvent : UnityEvent<GameObject> { }

public class EventListener : MonoBehaviour
{
    public Event gEvent;
    public UnityGameObjectEvent response = new UnityGameObjectEvent();

    private void OnEnable() {
        gEvent.Register(this);
    }

    private void OnDisable() {
        gEvent.Unregister(this);
    }

    // public void OnEventOccurs(GameObject gameObject, Image icon) {
    public void OnEventOccurs(GameObject gameObject) {
        // response.Invoke(gameObject, icon);
        response.Invoke(gameObject);
    }
}
