﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "NewEvent", menuName = "Game Event", order = 52)]
public class Event : ScriptableObject
{
    private List<EventListener> elisteners = new List<EventListener>();

    public void Register(EventListener listener) {
        elisteners.Add(listener);
    }

    public void Unregister(EventListener listener) {
        elisteners.Remove(listener);
    }

    // public void Occured(GameObject gameObject, Image icon) {
    public void Occured(GameObject gameObject) {
        for (int i = 0; i < elisteners.Count; i++) {
            // elisteners[i].OnEventOccurs(gameObject, icon);
            elisteners[i].OnEventOccurs(gameObject);
        }
    }
}
