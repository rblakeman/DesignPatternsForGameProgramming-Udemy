﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public sealed class GameEnvironment
{
    private static GameEnvironment instance;

    public List<GameObject> checkpoints = new List<GameObject>();
    public GameObject hidingSpot = null;

    public List<GameObject> Checkpoints {
        get { return checkpoints; }
    }

    public GameObject HidingSpot {
        get { return hidingSpot; }
    }

    public static GameEnvironment Singleton {
        get {
            if (instance == null) {
                instance = new GameEnvironment();

                instance.checkpoints.AddRange(GameObject.FindGameObjectsWithTag("Checkpoint"));
                instance.checkpoints = instance.checkpoints.OrderBy(item => item.name).ToList();

                instance.hidingSpot = GameObject.FindGameObjectWithTag("Safe");
            }

            return instance;
        }
    }
}
