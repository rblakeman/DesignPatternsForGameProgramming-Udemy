﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Running : State {
    public Running (GameObject _npc, NavMeshAgent _agent, Animator _animator, Transform _player)
                : base(_npc, _agent, _animator, _player) {
        name = STATE.RUNNING;
        agent.speed = 5f;
        agent.isStopped = false;
    }

    public override void Enter() {
        animator.SetTrigger("isRunning");

        base.Enter();
    }

    public override void Update() {
        agent.SetDestination(player.position);

        if (agent.hasPath) {
            if (playerAttackable()) {
                nextState = new Attacking(npc, agent, animator, player);
                stage = EVENT.EXIT;
            } else if (!playerVisible()) {
                nextState = new Walking(npc, agent, animator, player);
                stage = EVENT.EXIT;
            }
        }
    }

    public override void Exit() {
        animator.ResetTrigger("isRunning");

        base.Exit();
    }
}
