﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Idle : State {
    public Idle (GameObject _npc, NavMeshAgent _agent, Animator _animator, Transform _player)
                : base(_npc, _agent, _animator, _player) {
        name = STATE.IDLE;
    }

    public override void Enter() {
        animator.SetTrigger("isIdle");
        agent.isStopped = true;
        agent.speed = 0f;

        base.Enter();
    }

    public override void Update() {
        if (playerVisible()) {
            nextState = new Running(npc, agent, animator, player);
            stage = EVENT.EXIT;
        } else if (needToHide()) {
            nextState = new Hiding(npc, agent, animator, player);
            stage = EVENT.EXIT;
        } else if (Random.Range(0, 3000) < 1.0f) {
            nextState = new Walking(npc, agent, animator, player);
            stage = EVENT.EXIT;
        }
    }

    public override void Exit() {
        animator.ResetTrigger("isIdle");

        base.Exit();
    }
}
