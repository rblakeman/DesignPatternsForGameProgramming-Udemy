﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Walking : State {
    int currentIdx = -1;

    public Walking (GameObject _npc, NavMeshAgent _agent, Animator _animator, Transform _player)
                : base(_npc, _agent, _animator, _player) {
        name = STATE.WALKING;
        agent.speed = 2f;
        agent.isStopped = false;
    }

    public override void Enter() {
        float lowestDistance = Mathf.Infinity;
        for (int i = 0; i < GameEnvironment.Singleton.Checkpoints.Count; i++) {
            GameObject thisCheckpoint = GameEnvironment.Singleton.Checkpoints[i];
            float distanceToCheckpoint = Vector3.Distance(npc.transform.position, thisCheckpoint.transform.position);
            if (distanceToCheckpoint < lowestDistance) {
                currentIdx = i - 1;
                lowestDistance = distanceToCheckpoint;
            }
        }

        animator.SetTrigger("isWalking");

        base.Enter();
    }

    public override void Update() {
        if (agent.remainingDistance < 1f) {
            if (currentIdx >= GameEnvironment.Singleton.Checkpoints.Count - 1) {
                currentIdx = 0;
            } else {
                currentIdx++;
            }

            agent.SetDestination(GameEnvironment.Singleton.Checkpoints[currentIdx].transform.position);
        }

        if (playerVisible()) {
            nextState = new Running(npc, agent, animator, player);
            stage = EVENT.EXIT;
        } else if (needToHide()) {
            nextState = new Hiding(npc, agent, animator, player);
            stage = EVENT.EXIT;
        }
    }

    public override void Exit() {
        animator.ResetTrigger("isWalking");

        base.Exit();
    }
}
