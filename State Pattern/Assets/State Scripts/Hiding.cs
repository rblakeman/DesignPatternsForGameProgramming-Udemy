using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Hiding : State {
    public Hiding (GameObject _npc, NavMeshAgent _agent, Animator _animator, Transform _player)
                : base(_npc, _agent, _animator, _player) {
        name = STATE.HIDING;
        agent.speed = 6f;
        agent.isStopped = false;
    }

    public override void Enter() {
        animator.SetTrigger("isRunning");
        agent.SetDestination(GameEnvironment.Singleton.HidingSpot.transform.position);

        base.Enter();
    }

    public override void Update() {
        if (agent.remainingDistance < 1f) {
            nextState = new Idle(npc, agent, animator, player);
            stage = EVENT.EXIT;
        }
    }

    public override void Exit() {
        animator.ResetTrigger("isRunning");

        base.Exit();
    }
}
