﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Sleeping : State {
    public Sleeping (GameObject _npc, NavMeshAgent _agent, Animator _animator, Transform _player)
                : base(_npc, _agent, _animator, _player) {
        name = STATE.SLEEPING;
        agent.speed = 0f;
        agent.isStopped = true;
    }

    public override void Enter() {
        animator.SetTrigger("isSleeping");

        base.Enter();
    }

    public override void Update() {
        base.Update();
    }

    public override void Exit() {
        animator.ResetTrigger("isSleeping");

        base.Exit();
    }
}
