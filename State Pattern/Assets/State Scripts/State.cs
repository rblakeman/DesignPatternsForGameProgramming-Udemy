﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class State
{
    public enum STATE {
        IDLE, WALKING, RUNNING, ATTACKING, SLEEPING, HIDING
    };

    public enum EVENT {
        ENTER, UPDATE, EXIT
    };

    public STATE name;
    protected EVENT stage;
    protected GameObject npc;
    protected NavMeshAgent agent;
    protected Animator animator;
    protected Transform player;
    protected State nextState;

    float visibleDistance = 10.0f;
    float visibleAngle = 30.0f;
    float shootDistance = 7.0f;

    public State (GameObject _npc, NavMeshAgent _agent, Animator _animator, Transform _player) {
        npc = _npc;
        agent = _agent;
        animator = _animator;
        player = _player;
    }

    public virtual void Enter() {
        stage = EVENT.UPDATE;
    }

    public virtual void Update() {
        stage = EVENT.UPDATE;
    }

    public virtual void Exit() {
        stage = EVENT.EXIT;
    }

    public State Process() {
        switch (stage) {
            case EVENT.ENTER:
                Enter();
                break;
            case EVENT.UPDATE:
                Update();
                break;
            case EVENT.EXIT:
                Exit();
                return nextState;
        }

        return this;
    }

    public bool playerVisible() {
        Vector3 direction = player.position - npc.transform.position;
        float angle = Vector3.Angle(direction, npc.transform.forward);

        if (direction.magnitude <= visibleDistance && angle < visibleAngle) {
            return true;
        }

        return false;
    }

    public bool playerAttackable() {
        Vector3 direction = player.position - npc.transform.position;

        return direction.magnitude < shootDistance;
    }

    public bool needToHide() {
        Vector3 direction = npc.transform.position - player.position;
        float angle = Vector3.Angle(direction, npc.transform.forward);

        if (direction.magnitude <= 3.0f && angle < 30) {
            return true;
        }

        return false;
    }
}
