﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Attacking : State {
    float rotationSpeed = 2.0f;
    AudioSource shoot;

    public Attacking (GameObject _npc, NavMeshAgent _agent, Animator _animator, Transform _player)
                : base(_npc, _agent, _animator, _player) {
        name = STATE.ATTACKING;
        shoot = _npc.GetComponent<AudioSource>();
    }

    public override void Enter() {
        animator.SetTrigger("isShooting");
        agent.isStopped = true;
        shoot.Play();

        base.Enter();
    }

    public override void Update() {
        Vector3 direction = player.position - npc.transform.position;
        float angle = Vector3.Angle(direction, npc.transform.forward);
        direction.y = 0;
        npc.transform.rotation = Quaternion.Slerp(
            npc.transform.rotation,
            Quaternion.LookRotation(direction),
            Time.deltaTime * rotationSpeed
        );

        if (!playerAttackable()) {
            nextState = new Idle(npc, agent, animator, player);
            stage = EVENT.EXIT;
        }
    }

    public override void Exit() {
        animator.ResetTrigger("isShooting");
        shoot.Stop();

        base.Exit();
    }
}
