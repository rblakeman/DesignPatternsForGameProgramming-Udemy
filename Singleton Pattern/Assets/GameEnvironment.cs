﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class GameEnvironment
{
    private static GameEnvironment instance;

    private List<GameObject> obstacles = new List<GameObject>();
    private List<GameObject> goals = new List<GameObject>();

    public List<GameObject> Obstacles {
        get { return obstacles; }
    }

    public List<GameObject> Goals {
        get { return goals; }
    }

    public static GameEnvironment Singleton {
        get {
            if (instance == null) {
                instance = new GameEnvironment();

                instance.goals.AddRange(GameObject.FindGameObjectsWithTag("goal"));
            }
            return instance;
        }
    }

    public void AddObstacles(GameObject obstacle) {
        obstacles.Add(obstacle);
    }

    public void RemoveObstacles(GameObject obstacle) {
        int idx = obstacles.IndexOf(obstacle);

        obstacles.RemoveAt(idx);
        GameObject.Destroy(obstacle);
    }

    public void AddGoal(GameObject goal) {
        goals.Add(goal);
    }

    public GameObject GetRandomGoal() {
        return goals[Random.Range(0, goals.Count)];
    }
}
