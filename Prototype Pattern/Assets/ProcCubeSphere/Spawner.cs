﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject cubePrefab;
    public GameObject spherePrefab;
    public bool benchmarkMode = false;
    public int chanceOfSpawn = 10;

    // Start is called before the first frame update
    void Start()
    {
        if (benchmarkMode) {
            chanceOfSpawn = 100;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Random.Range(0,100) < chanceOfSpawn) {
            Instantiate(cubePrefab, this.transform.position, Quaternion.identity);
        } else if (Random.Range(0,100) < chanceOfSpawn) {
            Instantiate(spherePrefab, this.transform.position, Quaternion.identity);
        }
    }
}
