﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProcSphereSpawner : MonoBehaviour
{
    public bool benchmarkMode = false;
    public int chanceOfSpawn = 10;

    // Start is called before the first frame update
    void Start()
    {
        if (benchmarkMode) {
            chanceOfSpawn = 100;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Random.Range(0,100) < chanceOfSpawn) {
            // ProcSphere.CreateSphere(this.transform.position);
            ProcSphere.Clone(this.transform.position);
        }
    }
}
