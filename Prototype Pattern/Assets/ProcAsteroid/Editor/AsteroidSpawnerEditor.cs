﻿using System.IO;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ProcAsteroidSpawner))]
public class AsteroidSpawnerEditor : Editor
{
    string path;
    string assetPath;
    string fileName;

    void OnEnable() {
        path = Application.dataPath + "/ProcAsteroid/Asteroid";
        assetPath = "Assets/ProcAsteroid/Asteroid/";
        fileName = "asteroid_" + System.DateTime.Now.Ticks.ToString();
    }

    public override void OnInspectorGUI() {
        ProcAsteroidSpawner astSpawner = (ProcAsteroidSpawner) target;
        DrawDefaultInspector();

        if (GUILayout.Button("Create Asteroid")) {
            astSpawner.CreateAsteroid();
        }

        if (GUILayout.Button("Save Asteroid")) {
            System.IO.Directory.CreateDirectory(path);
            Mesh mesh = astSpawner.asteroid.GetComponent<MeshFilter>().sharedMesh;
            AssetDatabase.CreateAsset(mesh, assetPath + mesh.name + ".asset");
            AssetDatabase.SaveAssets();

            PrefabUtility.SaveAsPrefabAsset(astSpawner.asteroid, assetPath + fileName + ".prefab");
        }
    }
}
